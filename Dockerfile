FROM golang:latest as build

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download 

COPY . .

RUN go build -o fizzbuzz

EXPOSE 8080

CMD ["./fizzbuzz"]
