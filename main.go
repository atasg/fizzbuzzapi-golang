package main

import (
	"fizzbuzz/internal/fizzbuzz"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func CreateApp() *fiber.App{
	service := fizzbuzz.NewService()
	handler := fizzbuzz.NewHandler(service)
	app := fiber.New()
	handler.RegisterRoutes(app)
	return app

}


func main() {
	app := CreateApp()

	app.Use(cors.New(cors.Config{
		AllowOrigins: "http://127.0.0.1:3000",
		AllowHeaders: "*",
		AllowCredentials: true,
	}))
	app.Listen(":8080")
}