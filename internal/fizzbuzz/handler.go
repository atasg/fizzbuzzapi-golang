package fizzbuzz

import (
	"github.com/gofiber/fiber/v2"
	"strconv"
)
type HandlerService interface{
	FizzBuzz(from, to, fizz, buzz int64) *[]string

}
type Handler struct{
	Service HandlerService
}

func NewHandler(s HandlerService) *Handler{
	return &Handler{
		Service: s,
	}
}

func (h *Handler) RegisterRoutes(app *fiber.App){
	app.Get("/fizzbuzz/from::from-to::to-fizz::fizz-buzz::buzz",h.FizzBuzz)
}

func (h *Handler) FizzBuzz(c *fiber.Ctx) error{
	from,_ := strconv.ParseInt(c.Params("from"),10,64)
	to, _:= strconv.ParseInt(c.Params("to"),10,64)
	fizz, _ := strconv.ParseInt(c.Params("fizz"),10,64)
	buzz, _ := strconv.ParseInt(c.Params("buzz"),10,64)

	output := h.Service.FizzBuzz(from, to, fizz,buzz)

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{
		"fizzBuzz": &output,
	})
}