package fizzbuzz

import(
	"strconv"
)

type Service struct{

}

func NewService() *Service{
	return &Service{}
}

func (s *Service) FizzBuzz(from, to, fizz, buzz int64) *[]string{
	var sliceFizzBuzz []string

	for i :=from; i<=to; i++{
		if i%fizz == 0 && i%buzz != 0{
			sliceFizzBuzz = append(sliceFizzBuzz,"fizz")
		} else if i%fizz != 0 && i%buzz == 0{
			sliceFizzBuzz = append(sliceFizzBuzz,"buzz")
		} else if i%fizz == 0 && i%buzz == 0{
			sliceFizzBuzz = append(sliceFizzBuzz,"fizzbuzz")
		} else {
			sliceFizzBuzz = append(sliceFizzBuzz,strconv.FormatInt(i,10))
		}

	}
	return &sliceFizzBuzz
}